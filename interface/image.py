import matplotlib.pyplot as plt
import numpy as np
import typing as tp

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import SubplotParams
from tkinter import messagebox
import labelling.core as lc

from interface.color import Color, color2pixel
from labelling.core import classify_near_pixels, Point


class ImageView(FigureCanvasTkAgg):
    def __init__(self,
                 root,
                 frame):
        # make default image
        self.root = root
        self.image = np.random.rand(480, 640, 3).astype(np.int)
        self.edited_image = np.random.rand(480, 640, 3).astype(np.int)
        self.segmented_data = (np.zeros((480, 640)) - 1).astype(np.int)
        self.filename = None

        # make canvas
        subplotparams = SubplotParams(left=0,
                                      right=1,
                                      bottom=0,
                                      top=1,
                                      wspace=0.01,
                                      hspace=0)
        self.figure = plt.Figure(dpi=180,
                                 # frameon=False,
                                 figsize=[3.5*8/3, 3.5],
                                 subplotpars=subplotparams)
        gs = self.figure.add_gridspec(1, 2)

        super().__init__(self.figure, frame)
        self.pack = self.get_tk_widget().pack

        # make impedances animation
        ax = self.figure.add_subplot(gs[0, 0])
        ax.figure.canvas.mpl_connect('button_press_event', self.onclick)
        self.original_imshow = ax.imshow(self.image, aspect='equal')
        ax.axis('off')

        # make impedances animation
        self.editable_ax = self.figure.add_subplot(gs[0, 1])
        self.edited_imshow = self.editable_ax.imshow(self.image, aspect='equal')
        self.editable_ax.axis('off')

    def load(self, image):
        # load data
        self.image = image
        self.edited_image = np.copy(image)
        self.segmented_data = -np.ones(image.shape[:-1], dtype=int)
        self.update()

    def load_segmented(self, image, segmentation):
        self.image = image
        self.segmented_data = segmentation

        self.update_edit()
        self.update()

    def save(self, file):
        if file is not None:
            # save annotations as npy with same name as image
            np.savetxt(file, self.segmented_data)
            messagebox.showinfo("Info", "File saved.")

    def update(self):
        self.original_imshow.set_data(self.image)
        self.edited_imshow.set_data(self.edited_image)
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()

    def onclick(self, event):
        if event.inaxes == self.editable_ax:
            size, color = self.root.get_brush()
            x = int(event.xdata)
            y = int(event.ydata)
            fill_assist = self.root.use_fill_assist()
            if not fill_assist:
                self.common_brush(x, y, size, color)
            else:
                self.auto_fill_brush(x, y, size, color)


    def update_edit(self):
        for c in Color:
            mask = self.segmented_data == c.value
            self.edited_image[mask] = color2pixel[c.name]

    def common_brush(self, x, y, stroke, color):
        self.segmented_data[y - stroke//2: y + stroke//2 + 1,
                            x - stroke//2: x + stroke//2 + 1] = Color[color].value
        self.update_edit()
        self.update()

    def auto_fill_brush(self, x, y, stroke, color):
        partial_segmented = lc.classify_near_pixels(self.image, (x, y), stroke,
                                                     Color[color].value)

        has_value = (partial_segmented != -1)
        self.segmented_data = self.segmented_data*~has_value + has_value*partial_segmented
        self.update_edit()
        self.update()
