import enum


class Color(enum.Enum):
    WHITE = 0
    GREEN = 1
    BLUE = 2
    PINK = 3
    ROBOT_BLACK = 4
    NO_COLOR = 5


color2pixel = {
    'WHITE': (255, 255, 255),
    'GREEN': (0, 255, 0),
    'BLUE':  (0, 0, 255),
    'PINK':  (220, 100, 100),
    'ROBOT_BLACK': (70, 70, 70),
    'NO_COLOR': (0, 0, 0)
}