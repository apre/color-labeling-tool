from PIL import ImageTk, Image
import tkinter as tk
import os
import numpy as np
from tkinter import filedialog, messagebox
from interface.color import Color
from tkinter import PhotoImage
from itertools import chain
import cv2

from interface.image import ImageView


class Example():
    def __init__(self):
        super().__init__()
        self.root = tk.Tk()
        self.title_prefix = 'Segmentation Tool'
        self.root.title(self.title_prefix)
        self.top_frame = tk.Frame(self.root, height=5, width=50)
        self.bottom_frame = tk.Frame(self.root, height=10)
        self.image_view = ImageView(self, self.top_frame)
        self.toggle_button = tk.Button(master=self.bottom_frame,
                                       text="Fill assist",
                                       relief="raised",
                                       command=self.toggle)

        self.color = tk.StringVar(self.root, Color.WHITE.name)
        self.stroke = tk.IntVar(self.root, 1)
        self.fill_assist = tk.BooleanVar(False)
        self.image_on_canvas = None

        self.file_list = None
        self.file_index = -1
        self.saved = None

    def init_UI(self):
        self.top_frame.pack(side=tk.TOP)
        self.bottom_frame.pack(side=tk.BOTTOM, fill=tk.X)

        # left click binding
        self.image_view.pack(fill=tk.BOTH, expand=True)

        # change color to green
        tk.OptionMenu(self.bottom_frame, self.color, *[c.name for c in Color]).pack(side=tk.RIGHT)
        tk.Label(self.bottom_frame, text="Color: ").pack(side=tk.RIGHT)

        # stoke sizes
        tk.OptionMenu(self.bottom_frame, self.stroke, *[4*n+1 for n in range(10)]).pack(side=tk.RIGHT)
        tk.Label(self.bottom_frame, text="Stroke Size: ").pack(side=tk.RIGHT)

        # clear segmentation
        tk.Button(self.bottom_frame, text="Clear",
                  command=self.clear_segmentation).pack(side=tk.RIGHT)

        # save segmentation
        tk.Button(self.bottom_frame, text='Save', command=self.save_segmentation).pack(side=tk.RIGHT)

        # open directory
        tk.Button(self.bottom_frame, text='Open Directory', command=self.open_directory).pack(side=tk.RIGHT)

        # next image
        tk.Button(self.bottom_frame, text='Next', command=self.next_frame).pack(side=tk.RIGHT)

        tk.Button(self.bottom_frame, text="Previous",
                  command=self.previous_frame).pack(side=tk.RIGHT)

        # toggle button
        self.toggle_button.pack(side=tk.RIGHT)

        # main loop
        self.root.mainloop()

    def save_segmentation(self):
        if self.current_file:
            segmentation_name = Example.extension_to_npy(self.current_file)
            self.image_view.save(segmentation_name)

    def openfn(self):
        filename = filedialog.askopenfilename(title='open')
        return filename

    def open_directory(self):
        valid_ext = ["png", "jpg", "jpeg"]
        folder_name = filedialog.askdirectory(title='Select target directory')
        self.file_list = [os.path.join(folder_name, filename)
                          for filename in sorted(os.listdir(folder_name))
                          if any(filename.endswith(".{}".format(e)) for e in valid_ext)
                          ]
        self.file_index = -1
        self.next_frame()

    def clear_segmentation(self):
        if self.current_file:
            converted_file_path = self.extension_to_npy(self.current_file)
            if os.path.exists(converted_file_path):
                os.remove(converted_file_path)


    def load_segmentation_if_exists(self, resized_image):
        if self.current_file:
            segmentation_name = Example.extension_to_npy(self.current_file)
            if os.path.exists(segmentation_name):
                segmentation = np.loadtxt(segmentation_name)
                self.image_view.load_segmented(resized_image, segmentation)
                self.saved = True
            else:
                self.saved = False
                self.image_view.load(resized_image)

    def previous_frame(self):
        # self.save_segmentation()

        if self.file_list is not None:
            self.file_index -= 1

            while self.file_index >= 0:
                img = cv2.imread(self.current_file)
                if img is not None:
                    resized_image = cv2.resize(img, dsize=(480, 640),
                                               interpolation=cv2.INTER_CUBIC)
                    break
                else:
                    self.file_index -= 1
            else:
                self.file_index = -1
                messagebox.showinfo(title='End of folder',
                                    message='All files were read. Click "Next Frame" to start again')
                return
            self.set_title(self.file_list[self.file_index])
            self.load_segmentation_if_exists(resized_image)



    @property
    def current_file(self):
        return self.file_list[self.file_index] if self.file_list else None

    def next_frame(self):
        # FIXME: should do autosave??
        # self.save_segmentation()

        if self.file_list is not None:
            # update index
            self.file_index += 1

            # search for next image
            while self.file_index < len(self.file_list):
                img = cv2.imread(self.current_file)
                if img is not None:
                    resized_image = cv2.resize(img, dsize=(640, 480),
                                      interpolation=cv2.INTER_CUBIC)
                    break
                else:
                    self.file_index += 1
            else:
                self.file_index = -1
                messagebox.showinfo(title='End of folder',
                                    message='All files were read. Click "Next Frame" to start again')
                return
            # check if there is any saved instance
            self.set_title(self.file_list[self.file_index])
            self.load_segmentation_if_exists(resized_image)

    def get_brush(self):
        return self.stroke.get(), self.color.get()

    def use_fill_assist(self):
        return self.fill_assist.get()

    def toggle(self):
        if self.toggle_button.config('relief')[-1] == 'sunken':
            self.toggle_button.config(relief="raised")
            self.fill_assist.set(False)
        else:
            self.toggle_button.config(relief="sunken")
            self.fill_assist.set(True)

    def set_title(self, text):
        self.root.title(self.title_prefix + ' - ' + text)

    @staticmethod
    def extension_to_npy(path):
        pieces = path.split('.')
        pieces[-1] = '.npy'
        return ''.join(pieces)


def main():
    ex = Example()
    ex.init_UI()


if __name__ == '__main__':
    main()
