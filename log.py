

def pname(name, data, *args, **kwargs):
    print("[{}] {}".format(name, data.format(*args, **kwargs)))


def info(data, *args, **kwargs):
    pname("INFO", data, *args, **kwargs)


def debug(data, *args, **kwargs):
    pname("DEBUG", data, *args, **kwargs)

