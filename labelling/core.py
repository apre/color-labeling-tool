import numpy as np
import typing as tp
import queue
from itertools import chain
from collections import defaultdict
from functools import reduce
from log import info, debug

from concurrent.futures import ThreadPoolExecutor

Point = tp.Tuple[int, int]

DEFAULT_OFFSETS = (
    (1,0),
    (-1, 0),
    (0, 1),
    (0, -1)
)


def point_with_offset(pos: Point, offset: Point):
    """
    Adds offset to point
    :param pos:
    :param offset:
    :return:
    """
    return pos[0] + offset[0], pos[1] + offset[1]


def point_in_image(pos: Point, image: np.array):
    return pos[0] >= 0 and pos[0] < image.shape[1] and pos[1] >=0 and pos[1] < \
           image.shape[0]


def get_connected_positions(image: np.array, curr_pos: Point, offsets=DEFAULT_OFFSETS):
    """
    gets a tuple of next positions to get
    :param image: image to get near values
    :param curr_pos:
    :return: Iterable of next positions
    """
    ps = []
    for offset in offsets:
        p = point_with_offset(curr_pos, offset)
        if point_in_image(p, image):
            ps.append(p)

    return ps


def start_end_from_coord_diameter(x, diameter):
    return x - diameter//2, x + diameter//2


def validate_start_end(x_start, x_end, size):
    x_start = x_start if x_start >= 0 else 0
    x_end = x_end if x_end < size else size-1
    return x_start, x_end


def positions_inside_diameter(image: np.array, click_point: Point, diameter: float):
    x_start, x_end = start_end_from_coord_diameter(click_point[0], diameter)
    y_start, y_end = start_end_from_coord_diameter(click_point[1], diameter)

    x_start, x_end = validate_start_end(x_start, x_end, image.shape[1])
    y_start, y_end = validate_start_end(y_start, y_end, image.shape[0])

    x_space = np.array(range(x_start, x_end+1))
    y_space = np.array(range(y_start, y_end+1))

    x, y = np.meshgrid(x_space, y_space)
    return list(zip(x.flatten(), y.flatten()))

def get_border_from_coord_diameter(x, diameter, size):
    x_start, x_end = validate_start_end(x-diameter//2-1, x+diameter//2 + 1, size)
    return x_start, x_end

def position_border(image: np.array, click_point: Point, diameter: float):
    x_start, x_end = get_border_from_coord_diameter(click_point[0], diameter,
                                                    image.shape[1])
    y_start, y_end = get_border_from_coord_diameter(click_point[1], diameter,
                                                    image.shape[0])
    return list(chain(
        ((x_start, y) for y in range(y_start, y_end+1)),
        ((x_end, y) for y in range(y_start, y_end+1)),
        ((x, y_start) for x in range(x_start, x_end+1)),
        ((x, y_end) for x in range(x_start, x_end+1)),
    ))



def is_same_pixel_value(image: np.array, p1: Point, p2: Point):
    i1 = image[p1[1], p1[0], :]
    i2 = image[p2[1], p2[0], :]
    return np.equal(i1, i2).all()

def is_in_defined_colors(image, point, colors):
    return tuple(image[point[1], point[0], :]) in colors


def _process_point_bfs(image: np.array,
                       initial_point: Point,
                       colors: tp.Set[tp.Tuple],
                       visited: np.array
                       ):
    """
    Join colors that have same pixel values as the ones given in the image
    :param image: image to be processed
    :param compare_point: initial point to analyze
    :param curr_point: start position
    :param visited: array of visited pixels
    :return: visited array
    """
    q = queue.Queue()
    if is_in_defined_colors(image, initial_point, colors):
        q.put(initial_point)
    while not q.empty():
        curr_point = q.get()
        if visited[curr_point[1], curr_point[0]]:
            continue
        visited[curr_point[1], curr_point[0]] = True
        for child in get_connected_positions(image, curr_point):
            if point_in_image(child, image) and is_in_defined_colors(image,
            curr_point, colors):
                q.put(child)
    return visited


def process_point(image: np.array,
                  initial_point: Point,
                  colors: tp.Set[tp.Tuple],
                  visited,
                  color_id: int):
    visited = _process_point_bfs(image, initial_point, colors, visited)
    return visited


def merge_classification(arr1: np.array, arr2: np.array):
    return arr1 | arr2


def all_colors(image: np.array, points):
    d = set()
    for p in points:
        d.add(tuple(image[p[1], p[0]]))
    return d


def classify_near_pixels(image: np.array,
                         click_point: Point,
                         diameter: float,
                         color_id: int
                         ) -> np.array:
    """
    Classification of pixels near using DFS for searching for same pixels
    :param image:
    :param click_point:
    :param diameter:
    :param color_id:
    :return: array at same colors
    """
    debug(f"{image.shape}")
    info("defining points inside diameter")
    points = positions_inside_diameter(image, click_point, diameter)
    info("joining same colors inside diameter")
    all_colors_pixels = all_colors(image, points)
    info("getting border pixels")
    border = position_border(image, click_point, diameter)
    visited = np.zeros(image.shape[:-1], dtype=bool)
    for p in points:
        visited[p[1], p[0]] = True

    def proc(x):
        return process_point(image, x, all_colors_pixels, visited, color_id)
    info("joining classified images")
    reduced_values = reduce(merge_classification, map(proc, border)).astype(bool)
    output = reduced_values*color_id + (~reduced_values)*-1
    return output





