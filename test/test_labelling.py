import pytest
import numpy as np
import labelling.core as lc
import sys

def test_point_in_image():
    image = np.zeros((480, 640, 3))
    point = (20, 20)
    assert (lc.point_in_image(point, image))

    point = (-1, 2)
    assert (not lc.point_in_image(point, image))

    point = 480,2
    assert (not lc.point_in_image(point, image))


def test_points_inside_diameter():
    image = np.zeros((480, 640, 3))
    point = (20, 20)
    points = lc.positions_inside_diameter(image, point, diameter=10)
    should_be = [(y, x) for x in range(15, 26) for y in range(15, 26)]
    assert ({*points} == {*should_be})


def test_points_limited_diamter():
    image = np.zeros((480, 640, 3))
    point = (1, 1)
    points = lc.positions_inside_diameter(image, point, diameter=4)
    print(points)


def test_same_pixel_value():
    image = np.zeros((480, 640, 3))
    start_point=(1, 1)
    assert all(lc.is_same_pixel_value(image, start_point, (y, x))
               for x in range(640) for y in range(480))


def test_point_labelling():
    sample_image = np.random.randint(0, 10, size=(480, 640, 3))
    ans = lc.classify_near_pixels(sample_image, (100, 100), 6, 2)
    # print(ans)

    print((ans!=0).sum())
